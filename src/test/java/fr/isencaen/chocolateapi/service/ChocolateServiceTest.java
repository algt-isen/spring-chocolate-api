package fr.isencaen.chocolateapi.service;

import fr.isencaen.chocolateapi.exception.ChocolateFunctionalException;
import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.model.Chocolate;
import fr.isencaen.chocolateapi.model.ChocolateDto;
import fr.isencaen.chocolateapi.model.CreateChocolateDto;
import fr.isencaen.chocolateapi.repository.ChocolateRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ChocolateServiceTest {
    @InjectMocks
    private ChocolateService chocolateService;

    @Mock
    private ChocolateRepository chocolateRepository;

    @Test
    void getAllChocolates_withoutResult() {
        // Given

        // When
        List<ChocolateDto> result = chocolateService.getAllChocolates();

        // Then
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    void getAllChocolates_withResult() {
        // Given
        Mockito.when(chocolateRepository.findAll()).thenReturn(List.of(new Chocolate(1, "Snicker", "BARCODE")));

        // When
        List<ChocolateDto> result = chocolateService.getAllChocolates();

        // Then
        Assertions.assertEquals(List.of(new ChocolateDto(1, "Snicker", "BARCODE")), result);
    }

    @Test
    void createChocolate() throws ChocolateFunctionalException {
        // Given
        Mockito.when(chocolateRepository.save(new Chocolate("Bounty", "barcode")))
                .thenReturn(new Chocolate(14, "Bounty", "barcode"));

        // When
        ChocolateDto result = chocolateService.createChocolate(new CreateChocolateDto("Bounty", "barcode"));

        // Then
        Assertions.assertEquals(new ChocolateDto(14, "Bounty", "barcode"), result);
    }

    @Test
    void createChocolate_doublon() {
        // Given
        Mockito.when(chocolateRepository.existsByBarCode("barcode")).thenReturn(true);

        // When
        ChocolateFunctionalException exception = Assertions.assertThrows(
                ChocolateFunctionalException.class,
                () -> chocolateService.createChocolate(new CreateChocolateDto("Bounty", "barcode"))
        );

        // Then
        Assertions.assertEquals(new ChocolateFunctionalException("duplicate barcode", "DUPLICATE_BARCODE"), exception);
    }

    @Test
    void deleteChocolateById() {
        // When
        chocolateService.deleteChocolateById(18);

        // Then
        Mockito.verify(chocolateRepository, Mockito.times(1)).deleteById(18);
    }

    @Test
    void getOneChocolate_found() throws ChocolateNotFoundException {
        // Given
        Mockito.when(chocolateRepository.findById(118712))
                .thenReturn(Optional.of(new Chocolate(118712, "Mars", "BARCODE")));

        // When
        ChocolateDto result = chocolateService.getOneChocolate(118712);

        // Then
        Assertions.assertEquals(new ChocolateDto(118712, "Mars", "BARCODE"), result);
    }

    @Test
    void getOneChocolate_notFound() {
        // Given
        Mockito.when(chocolateRepository.findById(118712)).thenReturn(Optional.empty());

        // When
        ChocolateNotFoundException exception = Assertions.assertThrows(
                ChocolateNotFoundException.class,
                () -> chocolateService.getOneChocolate(118712)
        );

        // Then
        Assertions.assertEquals(new ChocolateNotFoundException("The chocolate 118712 was not found", "CHOCOLATE_NOT_FOUND"), exception);
    }
}
