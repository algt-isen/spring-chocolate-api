package fr.isencaen.chocolateapi.service;

import fr.isencaen.chocolateapi.repository.ChocolateRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

@SpringBootTest
class ChocolateServiceWithCacheTest {
    @Autowired
    private ChocolateService chocolateService;

    @MockBean
    private ChocolateRepository chocolateRepository;

    @Test
    void findChocolates_cached() {
        // Given
        Mockito.when(chocolateRepository.findAll()).thenReturn(List.of());

        // When
        chocolateService.getAllChocolates();
        chocolateService.getAllChocolates();

        // Then
        Mockito.verify(chocolateRepository, Mockito.times(1)).findAll();
    }
}

