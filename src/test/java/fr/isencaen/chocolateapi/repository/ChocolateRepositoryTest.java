package fr.isencaen.chocolateapi.repository;

import fr.isencaen.chocolateapi.model.Chocolate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

@DataJpaTest
public class ChocolateRepositoryTest {

    @Autowired
    private ChocolateRepository chocolateRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    void findAll_withoutResult() {
        // Given

        // When
        List<Chocolate> result = chocolateRepository.findAll();

        // Then
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    void findAll_withResult() {
        // Given
        testEntityManager.clear();
        Chocolate chocolatePersisted = testEntityManager.persist(new Chocolate("Bounty", "BARCODE"));

        // When
        List<Chocolate> result = chocolateRepository.findAll();
        
        // Then
        Assertions.assertEquals(List.of(new Chocolate(chocolatePersisted.getId(), "Bounty", "BARCODE")), result);
    }

    @Test
    void save() {
        // When
        Chocolate result = chocolateRepository.save(new Chocolate("Bounty", "barcode"));
        Chocolate chocolate = testEntityManager.find(Chocolate.class, 1);

        // Then
        Assertions.assertEquals(new Chocolate(1, "Bounty", "barcode"), result);
        Assertions.assertEquals(new Chocolate(1, "Bounty", "barcode"), chocolate);
    }

    @Test
    void existsByBarCode_withResult() {
        // Given
        testEntityManager.persist(new Chocolate("Bounty", "barCode1"));

        // When
        boolean result = chocolateRepository.existsByBarCode("barCode1");

        // Then
        Assertions.assertTrue(result);
    }

    @Test
    void existsByBarCode_withoutResult() {
        // Given

        // When
        boolean result = chocolateRepository.existsByBarCode("barCode1");

        // Then
        Assertions.assertFalse(result);
    }

    @Test
    void deleteById() {
        // Given
        Chocolate chocolateCreated = testEntityManager.persist(new Chocolate("Bounty", "barCode1"));

        // When
        chocolateRepository.deleteById(chocolateCreated.getId());

        // Then
        Assertions.assertNull(testEntityManager.find(Chocolate.class, chocolateCreated.getId()));
    }
}
