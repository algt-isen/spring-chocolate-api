package fr.isencaen.chocolateapi.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.model.ChocolateDto;
import fr.isencaen.chocolateapi.model.CreateChocolateDto;
import fr.isencaen.chocolateapi.service.ChocolateService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;
import java.util.Map;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;

@WebMvcTest(ChocolateController.class)
class ChocolateControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ChocolateService chocolateService;

    @Test
    @WithMockUser(username = "John Doe", roles = {"USER"})
    void getAllChocolates_emptyList() throws Exception {
        // Given
        Mockito.when(chocolateService.getAllChocolates()).thenReturn(List.of());

        // When
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.get("/v1/chocolates")
                        .contentType("application/json")
        ).andReturn().getResponse();

        // Then
        Assertions.assertEquals(200, response.getStatus());
        List<ChocolateDto> result = new ObjectMapper().readValue(response.getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    @WithMockUser(username = "John Doe", roles = {"USER"})
    void getAllChocolates_withList() throws Exception {
        // Given
        Mockito.when(chocolateService.getAllChocolates()).thenReturn(List.of(new ChocolateDto(1, "Bueno", "BARCODE")));

        // When
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.get("/v1/chocolates")
                        .contentType("application/json")
        ).andReturn().getResponse();

        // Then
        Assertions.assertEquals(200, response.getStatus());
        List<ChocolateDto> result = new ObjectMapper().readValue(response.getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(List.of(new ChocolateDto(1, "Bueno", "BARCODE")), result);
    }

    @Test
    @WithMockUser(username = "John Doe", roles = {"USER"})
    void createChocolate() throws Exception {
        // Given
        Mockito.when(chocolateService.createChocolate(new CreateChocolateDto("Bounty", "barcode")))
                .thenReturn(new ChocolateDto(14, "Bounty", "barcode"));

        // When
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.post("/v1/chocolates")
                        .with(csrf())
                        .content(new ObjectMapper().writeValueAsString(new CreateChocolateDto("Bounty", "barcode")))
                        .contentType("application/json")
        ).andReturn().getResponse();

        // Then
        Assertions.assertEquals(201, response.getStatus());
        ChocolateDto chocolateDto = new ObjectMapper().readValue(response.getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(new ChocolateDto(14, "Bounty", "barcode"), chocolateDto);
    }

    @Test
    @WithMockUser(username = "John Doe", roles = {"USER"})
    void createChocolate_withoutName() throws Exception {
        // Given

        // When
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.post("/v1/chocolates")
                        .with(csrf())
                        .content("{}")
                        .contentType("application/json")
        ).andReturn().getResponse();

        // Then
        Assertions.assertEquals(400, response.getStatus());
        Map<String, String> errors = new ObjectMapper().readValue(response.getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(Map.of("barCode", "BarCode is mandatory", "name", "Name is mandatory"), errors);
    }

    @Test
    @WithMockUser(username = "John Doe", roles = {"USER"})
    void deleteChocolate() throws Exception {
        // Given

        // When
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.delete("/v1/chocolates/{chocolateId}", 18)
                        .with(csrf())
        ).andReturn().getResponse();

        // Then
        Assertions.assertEquals(200, response.getStatus());
        Assertions.assertEquals(0, response.getContentLength());
        Mockito.verify(chocolateService, Mockito.times(1)).deleteChocolateById(18);
    }

    @Test
    @WithMockUser(username = "John Doe", roles = {"USER"})
    void getOneChocolate_found() throws Exception {
        // Given
        Mockito.when(chocolateService.getOneChocolate(666))
                .thenReturn(new ChocolateDto(666, "Shokobon", "BARCODE"));

        // When
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.get("/v1/chocolates/{chocolateId}", 666)
                        .contentType("application/json")
        ).andReturn().getResponse();

        // Then
        Assertions.assertEquals(200, response.getStatus());
        ChocolateDto result = new ObjectMapper().readValue(response.getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(new ChocolateDto(666, "Shokobon", "BARCODE"), result);
    }

    @Test
    @WithMockUser(username = "John Doe", roles = {"USER"})
    void getOneChocolate_notFound() throws Exception {
        // Given
        Mockito.when(chocolateService.getOneChocolate(777))
                .thenThrow(new ChocolateNotFoundException("Chocolate not found", "CHOCOLATE_NOT_FOUND"));

        // When
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.get("/v1/chocolates/{chocolateId}", 777)
                        .contentType("application/json")
        ).andReturn().getResponse();

        // Then
        Assertions.assertEquals(404, response.getStatus());
        Map<String, String> result = new ObjectMapper().readValue(response.getContentAsString(), new TypeReference<>() {
        });
        Assertions.assertEquals(Map.of("code", "CHOCOLATE_NOT_FOUND", "message", "Chocolate not found"), result);
    }
}
