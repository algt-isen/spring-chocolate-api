package fr.isencaen.chocolateapi.model;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

public record CreateDispenserDto(
        @Min(1)
        @NotNull
        Integer spirals,
        @Min(1)
        @NotNull
        Integer maxSpiralSize
) {
}
