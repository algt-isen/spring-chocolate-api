package fr.isencaen.chocolateapi.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "isen_spiral")
public class Spiral {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "dispenser_id")
    private Dispenser dispenser;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ChocolateSpiral> chocolateSpirals;

    private Integer size;

    public Spiral() {
    }

    public Spiral(Dispenser dispenser, Integer size, List<ChocolateSpiral> chocolateSpirals) {
        this.dispenser = dispenser;
        this.size = size;
        this.chocolateSpirals = chocolateSpirals;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Dispenser getDispenser() {
        return dispenser;
    }

    public void setDispenser(Dispenser dispenser) {
        this.dispenser = dispenser;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public List<ChocolateSpiral> getChocolateSpirals() {
        return chocolateSpirals;
    }

    public void setChocolateSpirals(List<ChocolateSpiral> chocolateSpirals) {
        this.chocolateSpirals = chocolateSpirals;
    }
}
