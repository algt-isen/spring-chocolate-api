package fr.isencaen.chocolateapi.model;

import jakarta.validation.constraints.NotBlank;

public record CreateChocolateDto(
        @NotBlank(message = "Name is mandatory")
        String name,

        @NotBlank(message = "BarCode is mandatory")
        String barCode
) {
}
