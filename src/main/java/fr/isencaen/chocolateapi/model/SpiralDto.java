package fr.isencaen.chocolateapi.model;

import java.util.List;

public record SpiralDto(
        Integer id,
        Integer maxSize,
        List<ChocolateDto> chocolates
) {
    public static SpiralDto of(Spiral spiral) {
        return new SpiralDto(
                spiral.getId(),
                spiral.getSize(),
                spiral.getChocolateSpirals().stream().map(
                        chocolateSpiral -> ChocolateDto.of(chocolateSpiral.getChocolate())
                ).toList()
        );
    }

}
