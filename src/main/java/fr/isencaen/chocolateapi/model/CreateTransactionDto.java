package fr.isencaen.chocolateapi.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record CreateTransactionDto(
        @NotNull
        Integer chocolateId,
        @NotBlank
        String currency
) {
}
