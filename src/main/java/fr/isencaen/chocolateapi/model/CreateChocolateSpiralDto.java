package fr.isencaen.chocolateapi.model;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.util.List;

public record CreateChocolateSpiralDto(
        @NotEmpty()
        @NotNull
        List<Integer> chocolateIds

) {
}
