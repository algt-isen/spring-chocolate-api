package fr.isencaen.chocolateapi.model;

import java.util.List;

public record DispenserDto(
        Integer id,
        List<SpiralDto> spirals
) {
    public static DispenserDto of(Dispenser dispenser) {
        return new DispenserDto(dispenser.getId(), dispenser.getSpirals().stream().map(SpiralDto::of).toList());
    }
}
