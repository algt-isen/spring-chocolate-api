package fr.isencaen.chocolateapi.model;

import jakarta.persistence.*;

import java.math.BigDecimal;

@Entity
@IdClass(ChocolatePriceId.class)
@Table(name = "isen_chocolate_price")
public class ChocolatePrice {
    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "chocolate_id", nullable = false)
    private Chocolate chocolate;
    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "dispenser_id", nullable = false)
    private Dispenser dispenser;
    private BigDecimal price;

    public ChocolatePrice() {
    }

    public ChocolatePrice(Chocolate chocolate, Dispenser dispenser, BigDecimal price) {
        this.chocolate = chocolate;
        this.dispenser = dispenser;
        this.price = price;
    }

    public Chocolate getChocolate() {
        return chocolate;
    }

    public void setChocolate(Chocolate chocolate) {
        this.chocolate = chocolate;
    }

    public Dispenser getDispenser() {
        return dispenser;
    }

    public void setDispenser(Dispenser dispenser) {
        this.dispenser = dispenser;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
