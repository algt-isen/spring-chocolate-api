package fr.isencaen.chocolateapi.model;

import java.math.BigDecimal;

public record ChocolatePriceDto(
        ChocolateDto chocolateDto,
        DispenserDto dispenserDto,
        BigDecimal price
) {
    public static ChocolatePriceDto of(ChocolatePrice chocolatePrice) {
        return new ChocolatePriceDto(
                ChocolateDto.of(chocolatePrice.getChocolate()),
                DispenserDto.of(chocolatePrice.getDispenser()),
                chocolatePrice.getPrice()
        );
    }

}
