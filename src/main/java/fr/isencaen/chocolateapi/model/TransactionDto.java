package fr.isencaen.chocolateapi.model;

import java.math.BigDecimal;
import java.time.LocalDate;

public record TransactionDto(
        int id,

        ChocolateDto chocolate,
        BigDecimal priceInEuro,
        BigDecimal priceInChange,
        String currency,
        LocalDate startDate,

        TransactionStatusDto status
) {

    public static TransactionDto of(Transaction transaction) {
        return new TransactionDto(
                transaction.getId(),
                ChocolateDto.of(transaction.getChocolate()),
                transaction.getPriceInEuro(),
                transaction.getPriceInChange(),
                transaction.getCurrency(),
                transaction.getStartDate(),
                TransactionStatusDto.of(transaction.getStatus())
        );
    }
}
