package fr.isencaen.chocolateapi.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;

public record CreateChocolatePriceDto(
        @NotNull
        BigDecimal price,

        @NotNull
        @NotBlank
        String currency
) {
}
