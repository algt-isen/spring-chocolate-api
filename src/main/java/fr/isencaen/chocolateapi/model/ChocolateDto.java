package fr.isencaen.chocolateapi.model;

public record ChocolateDto(
        int id,
        String name,
        String barCode
) {
    public static ChocolateDto of(Chocolate chocolate) {
        return new ChocolateDto(
                chocolate.getId(),
                chocolate.getName(),
                chocolate.getBarCode()
        );
    }
}
