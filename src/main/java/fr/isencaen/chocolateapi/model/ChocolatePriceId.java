package fr.isencaen.chocolateapi.model;

import java.io.Serializable;

public class ChocolatePriceId implements Serializable {
    private Chocolate chocolate;
    private Dispenser dispenser;

    public ChocolatePriceId() {
    }

    public ChocolatePriceId(Chocolate chocolate, Dispenser dispenser) {
        this.chocolate = chocolate;
        this.dispenser = dispenser;
    }

    public Chocolate getChocolate() {
        return chocolate;
    }

    public void setChocolate(Chocolate chocolate) {
        this.chocolate = chocolate;
    }

    public Dispenser getDispenser() {
        return dispenser;
    }

    public void setDispenser(Dispenser dispenser) {
        this.dispenser = dispenser;
    }
}

