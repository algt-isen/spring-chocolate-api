package fr.isencaen.chocolateapi.model;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "isen_transaction")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "chocolate_id")
    private Chocolate chocolate;
    private BigDecimal priceInEuro;
    private BigDecimal priceInChange;
    private String currency;
    private LocalDate startDate;
    @ManyToOne
    @JoinColumn(name = "status_code")
    @Enumerated(EnumType.STRING)
    private TransactionStatus status;

    public Transaction() {
    }

    public Transaction(Chocolate chocolate, BigDecimal priceInEuro, BigDecimal priceInChange, String currency, LocalDate startDate, TransactionStatus status) {
        this.chocolate = chocolate;
        this.priceInEuro = priceInEuro;
        this.priceInChange = priceInChange;
        this.currency = currency;
        this.startDate = startDate;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Chocolate getChocolate() {
        return chocolate;
    }

    public void setChocolate(Chocolate chocolate) {
        this.chocolate = chocolate;
    }

    public BigDecimal getPriceInEuro() {
        return priceInEuro;
    }

    public void setPriceInEuro(BigDecimal priceInEuro) {
        this.priceInEuro = priceInEuro;
    }

    public BigDecimal getPriceInChange() {
        return priceInChange;
    }

    public void setPriceInChange(BigDecimal priceInChange) {
        this.priceInChange = priceInChange;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }
}
