package fr.isencaen.chocolateapi.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "isen_dispenser")
public class Dispenser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Spiral> spirals;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ChocolatePrice> chocolatePrices;

    public Dispenser() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Spiral> getSpirals() {
        return spirals;
    }

    public void setSpirals(List<Spiral> spirals) {
        this.spirals = spirals;
    }

    public List<ChocolatePrice> getChocolatePrices() {
        return chocolatePrices;
    }

    public void setChocolatePrices(List<ChocolatePrice> chocolatePrices) {
        this.chocolatePrices = chocolatePrices;
    }
}
