package fr.isencaen.chocolateapi.model;

import jakarta.persistence.*;

@Entity
@Table(name = "isen_transaction_status")
public class TransactionStatus {
    @Id
    @Enumerated(EnumType.STRING)
    private TransactionStatusEnum code;

    public TransactionStatus() {
    }

    public TransactionStatus(TransactionStatusEnum code) {
        this.code = code;
    }

    public TransactionStatusEnum getCode() {
        return code;
    }

    public void setCode(TransactionStatusEnum code) {
        this.code = code;
    }
}
