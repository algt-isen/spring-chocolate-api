package fr.isencaen.chocolateapi.model;

import java.io.Serializable;

public class ChocolateSpiralId implements Serializable {
    private Chocolate chocolate;
    private Spiral spiral;
    private Integer position;

    public ChocolateSpiralId() {
    }

    public Chocolate getChocolate() {
        return chocolate;
    }

    public void setChocolate(Chocolate chocolate) {
        this.chocolate = chocolate;
    }

    public Spiral getSpiral() {
        return spiral;
    }

    public void setSpiral(Spiral spiral) {
        this.spiral = spiral;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }
}

