package fr.isencaen.chocolateapi.model;

import java.math.BigDecimal;
import java.util.Map;

public record RatesDto(
        String base,
        Map<String, BigDecimal> rates
) {
}
