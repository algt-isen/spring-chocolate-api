package fr.isencaen.chocolateapi.model;

public record UpdateTransactionDto(
        TransactionStatusEnum status
) {
}
