package fr.isencaen.chocolateapi.model;

public record ChocolateSpiralDto(
        ChocolateDto chocolateDto,
        SpiralDto spiralDto,
        Integer position
) {
    public static ChocolateSpiralDto of(ChocolateSpiral chocolateSpiral) {
        return new ChocolateSpiralDto(
                ChocolateDto.of(chocolateSpiral.getChocolate()),
                SpiralDto.of(chocolateSpiral.getSpiral()),
                chocolateSpiral.getPosition()
        );
    }

}
