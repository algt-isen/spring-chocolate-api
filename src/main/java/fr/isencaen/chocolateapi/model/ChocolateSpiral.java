package fr.isencaen.chocolateapi.model;

import jakarta.persistence.*;

@Entity
@IdClass(ChocolateSpiralId.class)
@Table(name = "isen_chocolate_spiral")
public class ChocolateSpiral {
    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "chocolate_id", nullable = false)
    private Chocolate chocolate;
    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "spiral_id")
    private Spiral spiral;
    @Id
    private Integer position;

    public ChocolateSpiral() {
    }

    public ChocolateSpiral(Chocolate chocolate, Spiral spiral, Integer position) {
        this.chocolate = chocolate;
        this.spiral = spiral;
        this.position = position;
    }

    public Spiral getSpiral() {
        return spiral;
    }

    public void setSpiral(Spiral spiral) {
        this.spiral = spiral;
    }

    public Chocolate getChocolate() {
        return chocolate;
    }

    public void setChocolate(Chocolate chocolate) {
        this.chocolate = chocolate;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

}
