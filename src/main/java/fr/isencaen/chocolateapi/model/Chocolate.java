package fr.isencaen.chocolateapi.model;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "isen_chocolate")
public class Chocolate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private String barCode;

    public Chocolate() {
    }

    public Chocolate(int id, String name, String barCode) {
        this.id = id;
        this.name = name;
        this.barCode = barCode;
    }

    public Chocolate(String name, String barCode) {
        this.name = name;
        this.barCode = barCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chocolate chocolate = (Chocolate) o;
        return id == chocolate.id && Objects.equals(name, chocolate.name) && Objects.equals(barCode, chocolate.barCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, barCode);
    }
}
