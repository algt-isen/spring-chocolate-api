package fr.isencaen.chocolateapi.model;

public enum TransactionStatusEnum {
    WAITING,
    CANCELLED,
    PAID,
    COMPLETED
}
