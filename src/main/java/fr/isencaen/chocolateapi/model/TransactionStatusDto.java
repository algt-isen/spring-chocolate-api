package fr.isencaen.chocolateapi.model;

public record TransactionStatusDto(
        TransactionStatusEnum code
) {
    public static TransactionStatusDto of(TransactionStatus status) {
        return new TransactionStatusDto(status.getCode());
    }
}
