package fr.isencaen.chocolateapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChocolateApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChocolateApiApplication.class, args);
	}

}
