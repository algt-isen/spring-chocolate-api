package fr.isencaen.chocolateapi.configuration;

import fr.isencaen.chocolateapi.model.TransactionStatus;
import fr.isencaen.chocolateapi.model.TransactionStatusEnum;
import fr.isencaen.chocolateapi.repository.ChocolateRepository;
import fr.isencaen.chocolateapi.repository.DispenserRepository;
import fr.isencaen.chocolateapi.repository.TransactionRepository;
import fr.isencaen.chocolateapi.repository.TransactionStatusRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * Component used to fill the database - only in tests / courses
 */
@Component
public class DataLoader implements ApplicationRunner {
    private final ChocolateRepository chocolateRepository;
    private final DispenserRepository dispenserRepository;
    private final TransactionStatusRepository transactionStatusRepository;
    private final TransactionRepository transactionRepository;

    public DataLoader(ChocolateRepository chocolateRepository, DispenserRepository dispenserRepository, TransactionStatusRepository transactionStatusRepository, TransactionRepository transactionRepository) {
        this.chocolateRepository = chocolateRepository;
        this.dispenserRepository = dispenserRepository;
        this.transactionStatusRepository = transactionStatusRepository;
        this.transactionRepository = transactionRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
//        Chocolate kitkat = new Chocolate("KitKat", "bc1");
//        kitkat = chocolateRepository.save(kitkat);
//        Chocolate bueno = new Chocolate("Bueno", "bc2");
//        bueno = chocolateRepository.save(bueno);
//        Chocolate maltesers = new Chocolate("Maltesers", "bc3");
//        maltesers = chocolateRepository.save(maltesers);
//        Chocolate bounty = new Chocolate("Bounty", "bc4");
//        bounty = chocolateRepository.save(bounty);
//
//        Dispenser dispenser1 = new Dispenser();
//        dispenser1.setSpirals(new ArrayList<>());
//        dispenser1.setChocolatePrices(new ArrayList<>());
//        dispenser1 = dispenserRepository.save(dispenser1);
//
//        dispenser1.getChocolatePrices().add(new ChocolatePrice(kitkat, dispenser1, BigDecimal.valueOf(1.78)));
//        dispenser1.getChocolatePrices().add(new ChocolatePrice(bueno, dispenser1, BigDecimal.valueOf(1.68)));
//
//        dispenser1.getSpirals().add(new Spiral(dispenser1, 10, new ArrayList<>()));
//        dispenser1.getSpirals().add(new Spiral(dispenser1, 10, new ArrayList<>()));
//        dispenser1.getSpirals().add(new Spiral(dispenser1, 10, new ArrayList<>()));
//        dispenser1 = dispenserRepository.save(dispenser1);
//
        TransactionStatus waitingTransactionStatus = new TransactionStatus(TransactionStatusEnum.WAITING);
        transactionStatusRepository.save(waitingTransactionStatus);
        TransactionStatus cancelledTransactionStatus = new TransactionStatus(TransactionStatusEnum.CANCELLED);
        transactionStatusRepository.save(cancelledTransactionStatus);
        TransactionStatus paidTransactionStatus = new TransactionStatus(TransactionStatusEnum.PAID);
        transactionStatusRepository.save(paidTransactionStatus);
        TransactionStatus completedTransactionStatus = new TransactionStatus(TransactionStatusEnum.COMPLETED);
        transactionStatusRepository.save(completedTransactionStatus);

//        transactionRepository.save(new Transaction(
//                kitkat,
//                BigDecimal.valueOf(1.56),
//                BigDecimal.valueOf(1.56),
//                "EUR",
//                LocalDate.of(2024, 1, 31),
//                acceptedTransactionStatus
//        ));
//        transactionRepository.save(new Transaction(
//                kitkat,
//                BigDecimal.valueOf(1.56),
//                BigDecimal.valueOf(1.56),
//                "EUR",
//                LocalDate.of(2024, 1, 31),
//                acceptedTransactionStatus
//        ));
//        transactionRepository.save(new Transaction(
//                kitkat,
//                BigDecimal.valueOf(1.56),
//                BigDecimal.valueOf(1.56),
//                "EUR",
//                LocalDate.of(2024, 1, 31),
//                acceptedTransactionStatus
//        ));
//        transactionRepository.save(new Transaction(
//                kitkat,
//                BigDecimal.valueOf(1.56),
//                BigDecimal.valueOf(1.56),
//                "EUR",
//                LocalDate.of(2024, 1, 31),
//                acceptedTransactionStatus
//        ));
//        transactionRepository.save(new Transaction(
//                kitkat,
//                BigDecimal.valueOf(1.56),
//                BigDecimal.valueOf(1.56),
//                "EUR",
//                LocalDate.of(2024, 1, 31),
//                acceptedTransactionStatus
//        ));
//        transactionRepository.save(new Transaction(
//                kitkat,
//                BigDecimal.valueOf(1.56),
//                BigDecimal.valueOf(1.56),
//                "EUR",
//                LocalDate.of(2024, 1, 31),
//                acceptedTransactionStatus
//        ));
//        transactionRepository.save(new Transaction(
//                kitkat,
//                BigDecimal.valueOf(1.56),
//                BigDecimal.valueOf(1.56),
//                "EUR",
//                LocalDate.of(2024, 1, 31),
//                acceptedTransactionStatus
//        ));
//        transactionRepository.save(new Transaction(
//                kitkat,
//                BigDecimal.valueOf(1.56),
//                BigDecimal.valueOf(1.56),
//                "EUR",
//                LocalDate.of(2024, 1, 31),
//                acceptedTransactionStatus
//        ));
//        transactionRepository.save(new Transaction(
//                kitkat,
//                BigDecimal.valueOf(1.56),
//                BigDecimal.valueOf(1.56),
//                "EUR",
//                LocalDate.of(2024, 1, 31),
//                acceptedTransactionStatus
//        ));
//        transactionRepository.save(new Transaction(
//                kitkat,
//                BigDecimal.valueOf(1.56),
//                BigDecimal.valueOf(1.56),
//                "EUR",
//                LocalDate.of(2024, 1, 31),
//                acceptedTransactionStatus
//        ));
    }
}
