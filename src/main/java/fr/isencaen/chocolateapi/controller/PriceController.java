package fr.isencaen.chocolateapi.controller;

import fr.isencaen.chocolateapi.exception.ChocolateBadRequestException;
import fr.isencaen.chocolateapi.exception.ChocolateFunctionalException;
import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.exception.ChocolateTechnicalException;
import fr.isencaen.chocolateapi.service.PriceService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
public class PriceController {
    private final PriceService priceService;

    public PriceController(PriceService priceService) {
        this.priceService = priceService;
    }

    @PostMapping("v1/dispensers/{dispenserId}/spirals/{spiralId}")
    @ResponseStatus(HttpStatus.CREATED)
    public BigDecimal getChocolatePrice(
            @PathVariable Integer dispenserId,
            @PathVariable Integer spiralId,
            @RequestParam(required = false) String currency
    ) throws ChocolateFunctionalException, ChocolateTechnicalException, ChocolateNotFoundException, ChocolateBadRequestException {
        return priceService.getChocolatePrinceInCurrency(dispenserId, spiralId, currency);
    }

}
