package fr.isencaen.chocolateapi.controller;

import fr.isencaen.chocolateapi.exception.ChocolateBadRequestException;
import fr.isencaen.chocolateapi.exception.ChocolateFunctionalException;
import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.exception.ChocolateTechnicalException;
import fr.isencaen.chocolateapi.service.StastisticsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDate;

@RestController
public class StatisticsController {
    private final StastisticsService stastisticsService;

    public StatisticsController(StastisticsService stastisticsService) {
        this.stastisticsService = stastisticsService;
    }

    @GetMapping("v1/statistics/sales")
    public BigDecimal getSales(
            @RequestParam(required = false) LocalDate day
    ) throws ChocolateFunctionalException, ChocolateTechnicalException, ChocolateNotFoundException, ChocolateBadRequestException {
        return stastisticsService.getSales(day);
    }

}
