package fr.isencaen.chocolateapi.controller;

import fr.isencaen.chocolateapi.exception.ChocolateFunctionalException;
import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.model.ChocolateSpiralDto;
import fr.isencaen.chocolateapi.model.CreateChocolateSpiralDto;
import fr.isencaen.chocolateapi.service.ChocolateSpiralService;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ChocolateSpiralController {

    private final ChocolateSpiralService chocolateSpiralService;

    public ChocolateSpiralController(ChocolateSpiralService chocolateSpiralService) {
        this.chocolateSpiralService = chocolateSpiralService;
    }

    @PostMapping("v1/spirals/{spiralId}/chocolates")
    public List<ChocolateSpiralDto> createChocolateSpiral(
            @PathVariable Integer spiralId,
            @Valid @RequestBody CreateChocolateSpiralDto createChocolateSpiralDto
    ) throws ChocolateNotFoundException, ChocolateFunctionalException {
        return chocolateSpiralService.createChocolateSpiral(spiralId, createChocolateSpiralDto);
    }

    @DeleteMapping("v1/spirals/{spiralId}/chocolates/FIRST")
    public void deleteFirstChocolateSpiral(
            @PathVariable Integer spiralId
    ) throws ChocolateNotFoundException {
        chocolateSpiralService.deleteFirstChocolateSpiral(spiralId);
    }

    @DeleteMapping("v1/spirals/{spiralId}/chocolates/ALL")
    public void deleteAllChocolateSpiral(
            @PathVariable Integer spiralId
    ) throws ChocolateNotFoundException {
        chocolateSpiralService.deleteAllChocolatesSpiral(spiralId);
    }

}
