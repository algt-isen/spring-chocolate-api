package fr.isencaen.chocolateapi.controller;

import fr.isencaen.chocolateapi.exception.ChocolateBadRequestException;
import fr.isencaen.chocolateapi.exception.ChocolateFunctionalException;
import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.exception.ChocolateTechnicalException;
import fr.isencaen.chocolateapi.model.CreateTransactionDto;
import fr.isencaen.chocolateapi.model.TransactionDto;
import fr.isencaen.chocolateapi.model.UpdateTransactionDto;
import fr.isencaen.chocolateapi.service.TransactionService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class TransactionController {
    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping("v1/dispensers/{dispenserId}/transactions")
    @ResponseStatus(HttpStatus.CREATED)
    public TransactionDto createTransaction(
            @PathVariable Integer dispenserId,
            @Valid @RequestBody CreateTransactionDto createTransactionDto
    ) throws ChocolateBadRequestException, ChocolateFunctionalException, ChocolateTechnicalException, ChocolateNotFoundException {
        return transactionService.createTransaction(dispenserId, createTransactionDto);
    }

    @PatchMapping("v1/transactions/{transactionId}")
    public TransactionDto updateTransaction(
            @PathVariable Integer transactionId,
            @Valid @RequestBody UpdateTransactionDto updateTransactionDto
    ) throws ChocolateTechnicalException, ChocolateNotFoundException {
        return transactionService.updateTransaction(transactionId, updateTransactionDto);
    }

}
