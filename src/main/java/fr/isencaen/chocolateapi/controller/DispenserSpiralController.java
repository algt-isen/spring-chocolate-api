package fr.isencaen.chocolateapi.controller;

import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.model.CreateDispenserSpiralDto;
import fr.isencaen.chocolateapi.model.SpiralDto;
import fr.isencaen.chocolateapi.service.DispenserSpiralService;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

@RestController
public class DispenserSpiralController {
    private final DispenserSpiralService dispenserSpiralService;

    public DispenserSpiralController(DispenserSpiralService dispenserSpiralService) {
        this.dispenserSpiralService = dispenserSpiralService;
    }

    @PostMapping("v1/dispensers/{dispenserId}/spirals")
    public SpiralDto createDispenserSpiral(
            @PathVariable Integer dispenserId,
            @Valid @RequestBody CreateDispenserSpiralDto createDispenserSpiralDto
    ) throws ChocolateNotFoundException {
        return dispenserSpiralService.createDispenserSpiral(dispenserId, createDispenserSpiralDto);
    }

    @DeleteMapping("v1/dispensers/{dispenserId}/spirals/{spiralId}")
    public void deleteDispenserSpiral(
            @PathVariable Integer dispenserId,
            @PathVariable Integer spiralId
    ) throws ChocolateNotFoundException {
        dispenserSpiralService.deleteDispenserSpiral(dispenserId, spiralId);
    }
}
