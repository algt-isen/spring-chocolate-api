package fr.isencaen.chocolateapi.controller;

import fr.isencaen.chocolateapi.exception.ChocolateFunctionalException;
import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.model.ChocolatePriceDto;
import fr.isencaen.chocolateapi.model.CreateChocolatePriceDto;
import fr.isencaen.chocolateapi.service.ChocolatePriceService;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

@RestController
public class ChocolatePriceController {

    private final ChocolatePriceService chocolatePriceService;

    public ChocolatePriceController(ChocolatePriceService chocolatePriceService) {
        this.chocolatePriceService = chocolatePriceService;
    }

    @GetMapping("v1/dispensers/{dispenserId}/chocolates/{chocolateId}/price")
    public ChocolatePriceDto getChocolatePrice(
            @PathVariable Integer dispenserId,
            @PathVariable Integer chocolateId
    ) throws ChocolateNotFoundException {
        return chocolatePriceService.getChocolatePrice(dispenserId, chocolateId);
    }

    @PostMapping("v1/dispensers/{dispenserId}/chocolates/{chocolateId}/price")
    public ChocolatePriceDto createChocolatePrice(
            @PathVariable Integer dispenserId,
            @PathVariable Integer chocolateId,
            @Valid @RequestBody CreateChocolatePriceDto createChocolatePriceDto
    ) throws ChocolateNotFoundException, ChocolateFunctionalException {
        return chocolatePriceService.createChocolatePrice(dispenserId, chocolateId, createChocolatePriceDto);
    }

}
