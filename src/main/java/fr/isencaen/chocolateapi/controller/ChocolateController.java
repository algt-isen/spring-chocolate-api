package fr.isencaen.chocolateapi.controller;

import fr.isencaen.chocolateapi.exception.ChocolateFunctionalException;
import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.exception.NotImplementedException;
import fr.isencaen.chocolateapi.model.ChocolateDto;
import fr.isencaen.chocolateapi.model.CreateChocolateDto;
import fr.isencaen.chocolateapi.service.ChocolateService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ChocolateController {

    private final ChocolateService chocolateService;

    public ChocolateController(ChocolateService chocolateService) {
        this.chocolateService = chocolateService;
    }

    @GetMapping("v1/chocolates")
    public List<ChocolateDto> getAllChocolates() {
        return chocolateService.getAllChocolates();
    }

    @Operation(summary = "Create a chocolate")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created the chocolate", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ChocolateDto.class))}),
            @ApiResponse(responseCode = "409", description = "Duplicate barcode", content = @Content)
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("v1/chocolates")
    public ChocolateDto createChocolate(
            @Valid @RequestBody CreateChocolateDto createChocolateDto
    ) throws ChocolateFunctionalException {
        return chocolateService.createChocolate(createChocolateDto);
    }

    @DeleteMapping("v1/chocolates/{chocolateId}")
    public void deleteChocolate(
            @PathVariable Integer chocolateId
    ) {
        chocolateService.deleteChocolateById(chocolateId);
    }

    @GetMapping("v1/chocolates/{chocolateId}")
    public ChocolateDto getOneChocolate(
            @PathVariable Integer chocolateId
    ) throws NotImplementedException, ChocolateNotFoundException {
        return chocolateService.getOneChocolate(chocolateId);
    }
}
