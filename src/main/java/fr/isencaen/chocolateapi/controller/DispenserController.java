package fr.isencaen.chocolateapi.controller;

import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.model.CreateDispenserDto;
import fr.isencaen.chocolateapi.model.DispenserDto;
import fr.isencaen.chocolateapi.service.DispenserService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DispenserController {
    private final DispenserService dispenserService;

    public DispenserController(DispenserService dispenserService) {
        this.dispenserService = dispenserService;
    }


    @GetMapping("v1/dispensers")
    public List<DispenserDto> getAllDispensers() {
        return dispenserService.getAllDispensers();
    }

    @PostMapping("v1/dispensers")
    @ResponseStatus(HttpStatus.CREATED)
    public DispenserDto createDispenser(
            @Valid @RequestBody CreateDispenserDto createDispenserDto
    ) {
        return dispenserService.createDispenser(createDispenserDto);
    }

    @GetMapping("v1/dispensers/{dispenserId}")
    public DispenserDto getOneDispenser(
            @PathVariable Integer dispenserId
    ) throws ChocolateNotFoundException {
        return dispenserService.getOneDispenser(dispenserId);
    }

    @DeleteMapping("v1/dispensers/{dispenserId}")
    public void deleteDispenser(
            @PathVariable Integer dispenserId
    ) {
        dispenserService.deleteDispenserById(dispenserId);
    }


}
