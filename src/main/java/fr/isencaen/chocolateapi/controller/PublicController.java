package fr.isencaen.chocolateapi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PublicController {
    @GetMapping("v1/public")
    public Integer getPublicData() {
        return 9;
    }
}
