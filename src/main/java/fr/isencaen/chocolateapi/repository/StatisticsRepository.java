package fr.isencaen.chocolateapi.repository;

import fr.isencaen.chocolateapi.model.TransactionStatusEnum;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Component
public class StatisticsRepository {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public StatisticsRepository(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public BigDecimal getSales(LocalDate day) {
        String query = "SELECT NVL(SUM(price_in_euro), 0) FROM isen_transaction WHERE start_date = :startDate AND status_code IN (:statusCodes)";
        SqlParameterSource namedParameters = new MapSqlParameterSource()
                .addValue("startDate", day)
                .addValue("statusCodes", List.of(TransactionStatusEnum.PAID.name(), TransactionStatusEnum.COMPLETED.name()));
        return namedParameterJdbcTemplate.queryForObject(query, namedParameters, BigDecimal.class);
    }
}
