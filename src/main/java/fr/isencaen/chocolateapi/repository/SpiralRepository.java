package fr.isencaen.chocolateapi.repository;

import fr.isencaen.chocolateapi.model.Spiral;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpiralRepository extends JpaRepository<Spiral, Integer> {
}
