package fr.isencaen.chocolateapi.repository;

import fr.isencaen.chocolateapi.model.TransactionStatus;
import fr.isencaen.chocolateapi.model.TransactionStatusEnum;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionStatusRepository extends JpaRepository<TransactionStatus, TransactionStatusEnum> {
}
