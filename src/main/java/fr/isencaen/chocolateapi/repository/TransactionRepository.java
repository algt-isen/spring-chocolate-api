package fr.isencaen.chocolateapi.repository;

import fr.isencaen.chocolateapi.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
}
