package fr.isencaen.chocolateapi.repository;

import fr.isencaen.chocolateapi.model.ChocolatePrice;
import fr.isencaen.chocolateapi.model.ChocolatePriceId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChocolatePriceRepository extends JpaRepository<ChocolatePrice, ChocolatePriceId> {
}
