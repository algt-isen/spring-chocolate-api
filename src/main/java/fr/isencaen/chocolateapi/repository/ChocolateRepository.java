package fr.isencaen.chocolateapi.repository;

import fr.isencaen.chocolateapi.model.Chocolate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChocolateRepository extends JpaRepository<Chocolate, Integer> {
    boolean existsByBarCode(String barCode);
}
