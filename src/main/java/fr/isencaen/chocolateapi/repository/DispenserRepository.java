package fr.isencaen.chocolateapi.repository;

import fr.isencaen.chocolateapi.model.Dispenser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DispenserRepository extends JpaRepository<Dispenser, Integer> {
}
