package fr.isencaen.chocolateapi.service;

import fr.isencaen.chocolateapi.repository.StatisticsRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;

@Service
public class StastisticsService {
    private final StatisticsRepository statisticsRepository;

    public StastisticsService(StatisticsRepository statisticsRepository) {
        this.statisticsRepository = statisticsRepository;
    }

    public BigDecimal getSales(LocalDate day) {
        return statisticsRepository.getSales(day);
    }
}
