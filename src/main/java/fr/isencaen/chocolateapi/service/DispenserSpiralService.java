package fr.isencaen.chocolateapi.service;

import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.model.CreateDispenserSpiralDto;
import fr.isencaen.chocolateapi.model.Dispenser;
import fr.isencaen.chocolateapi.model.Spiral;
import fr.isencaen.chocolateapi.model.SpiralDto;
import fr.isencaen.chocolateapi.repository.DispenserRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class DispenserSpiralService {
    private final DispenserRepository dispenserRepository;

    public DispenserSpiralService(DispenserRepository dispenserRepository) {
        this.dispenserRepository = dispenserRepository;
    }

    public SpiralDto createDispenserSpiral(Integer dispenserId, CreateDispenserSpiralDto createDispenserSpiralDto) throws ChocolateNotFoundException {
        Dispenser dispenser = this.dispenserRepository.findById(dispenserId)
                .orElseThrow(() -> new ChocolateNotFoundException(String.format("The dispenser %s was not found", dispenserId), "DISPENSER_NOT_FOUND"));
        Spiral spiral = new Spiral(dispenser, createDispenserSpiralDto.maxSpiralSize(), new ArrayList<>());
        dispenser.getSpirals().add(spiral);
        this.dispenserRepository.save(dispenser);
        return SpiralDto.of(spiral);
    }

    public void deleteDispenserSpiral(Integer dispenserId, Integer spiralId) throws ChocolateNotFoundException {
        Dispenser dispenser = this.dispenserRepository.findById(dispenserId)
                .orElseThrow(() -> new ChocolateNotFoundException(String.format("The dispenser %s was not found", dispenserId), "DISPENSER_NOT_FOUND"));
        Optional<Spiral> optionalSpiralToDelete = dispenser.getSpirals().stream().filter(spirals -> spirals.getId() == spiralId).findFirst();
        optionalSpiralToDelete.ifPresent(spiral -> dispenser.getSpirals().remove(spiral));
        this.dispenserRepository.save(dispenser);
    }
}
