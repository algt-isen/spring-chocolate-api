package fr.isencaen.chocolateapi.service;

import fr.isencaen.chocolateapi.exception.ChocolateFunctionalException;
import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.model.*;
import fr.isencaen.chocolateapi.repository.ChocolateRepository;
import fr.isencaen.chocolateapi.repository.SpiralRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChocolateSpiralService {
    private final ChocolateRepository chocolateRepository;
    private final SpiralRepository spiralRepository;

    public ChocolateSpiralService(ChocolateRepository chocolateRepository, SpiralRepository spiralRepository) {
        this.chocolateRepository = chocolateRepository;
        this.spiralRepository = spiralRepository;
    }

    public List<ChocolateSpiralDto> createChocolateSpiral(Integer spiralId, CreateChocolateSpiralDto createChocolateSpiralDto) throws ChocolateNotFoundException, ChocolateFunctionalException {
        Spiral spiral = this.spiralRepository.findById(spiralId)
                .orElseThrow(() -> new ChocolateNotFoundException(String.format("The spiral %s was not found", spiralId), "SPIRAL_NOT_FOUND"));

        if (spiral.getChocolateSpirals().size() + createChocolateSpiralDto.chocolateIds().size() > spiral.getSize()) {
            throw new ChocolateFunctionalException("The spiral is already full.", "SPIRAL_FULL");
        }

        for (Integer chocolateId : createChocolateSpiralDto.chocolateIds()) {
            Chocolate chocolate = this.chocolateRepository.findById(chocolateId)
                    .orElseThrow(() -> new ChocolateNotFoundException(String.format("The chocolate %s was not found", chocolateId), "CHOCOLATE_NOT_FOUND"));
            ChocolateSpiral chocolateSpiral = new ChocolateSpiral(chocolate, spiral, spiral.getChocolateSpirals().size() + 1);
            spiral.getChocolateSpirals().add(chocolateSpiral);
        }

        this.spiralRepository.save(spiral);

        return spiral.getChocolateSpirals().stream().map(ChocolateSpiralDto::of).toList();
    }

    public void deleteFirstChocolateSpiral(Integer spiralId) throws ChocolateNotFoundException {
        Spiral spiral = this.spiralRepository.findById(spiralId)
                .orElseThrow(() -> new ChocolateNotFoundException(String.format("The spiral %s was not found", spiralId), "SPIRAL_NOT_FOUND"));

        if (!spiral.getChocolateSpirals().isEmpty()) {
            spiral.getChocolateSpirals().removeLast();
            this.spiralRepository.save(spiral);
        }
    }

    public void deleteAllChocolatesSpiral(Integer spiralId) throws ChocolateNotFoundException {
        Spiral spiral = this.spiralRepository.findById(spiralId)
                .orElseThrow(() -> new ChocolateNotFoundException(String.format("The spiral %s was not found", spiralId), "SPIRAL_NOT_FOUND"));

        if (!spiral.getChocolateSpirals().isEmpty()) {
            spiral.getChocolateSpirals().removeAll(spiral.getChocolateSpirals());
            this.spiralRepository.save(spiral);
        }
    }
}
