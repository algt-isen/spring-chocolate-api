package fr.isencaen.chocolateapi.service;

import fr.isencaen.chocolateapi.exception.ChocolateBadRequestException;
import fr.isencaen.chocolateapi.exception.ChocolateFunctionalException;
import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.exception.ChocolateTechnicalException;
import fr.isencaen.chocolateapi.model.*;
import fr.isencaen.chocolateapi.repository.ChocolateRepository;
import fr.isencaen.chocolateapi.repository.TransactionRepository;
import fr.isencaen.chocolateapi.repository.TransactionStatusRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class TransactionService {
    private final TransactionRepository transactionRepository;
    private final ChocolateRepository chocolateRepository;
    private final TransactionStatusRepository transactionStatusRepository;
    private final PriceService priceService;

    public TransactionService(TransactionRepository transactionRepository, ChocolateRepository chocolateRepository, TransactionStatusRepository transactionStatusRepository, PriceService priceService) {
        this.transactionRepository = transactionRepository;
        this.chocolateRepository = chocolateRepository;
        this.transactionStatusRepository = transactionStatusRepository;
        this.priceService = priceService;
    }

    public TransactionDto createTransaction(Integer dispenserId, CreateTransactionDto createTransactionDto) throws ChocolateBadRequestException, ChocolateNotFoundException, ChocolateTechnicalException, ChocolateFunctionalException {
        Chocolate chocolate = this.chocolateRepository.findById(createTransactionDto.chocolateId())
                .orElseThrow(() -> new ChocolateBadRequestException(String.format("The chocolate %s was not found", createTransactionDto.chocolateId()), "CHOCOLATE_NOT_FOUND"));

        TransactionStatus transactionStatus = transactionStatusRepository.findById(TransactionStatusEnum.WAITING)
                .orElseThrow(() -> new ChocolateTechnicalException(String.format("Transaction status %s was not found", TransactionStatusEnum.WAITING), "TRANSACTION_STATUS_NOT_FOUND"));

        Transaction transaction = new Transaction();
        transaction.setChocolate(chocolate);
        transaction.setPriceInEuro(priceService.getChocolatePrice(dispenserId, createTransactionDto.chocolateId()));
        transaction.setPriceInChange(priceService.getChocolatePrinceInCurrency(dispenserId, createTransactionDto.chocolateId(), createTransactionDto.currency()));
        transaction.setCurrency(createTransactionDto.currency());
        transaction.setStartDate(LocalDate.now());
        transaction.setStatus(transactionStatus);

        transaction = transactionRepository.save(transaction);

        return TransactionDto.of(transaction);
    }

    public TransactionDto updateTransaction(Integer transactionId, UpdateTransactionDto updateTransactionDto) throws ChocolateTechnicalException, ChocolateNotFoundException {
        Transaction transaction = transactionRepository.findById(transactionId)
                .orElseThrow(() -> new ChocolateNotFoundException(String.format("Transaction %s was not found", transactionId), "TRANSACTION_NOT_FOUND"));

        TransactionStatus transactionStatus = transactionStatusRepository.findById(updateTransactionDto.status())
                .orElseThrow(() -> new ChocolateTechnicalException(String.format("Transaction status %s was not found", updateTransactionDto.status()), "TRANSACTION_STATUS_NOT_FOUND"));

        transaction.setStatus(transactionStatus);

        return TransactionDto.of(transactionRepository.save(transaction));
    }
}
