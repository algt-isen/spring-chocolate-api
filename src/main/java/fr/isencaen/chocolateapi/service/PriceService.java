package fr.isencaen.chocolateapi.service;

import fr.isencaen.chocolateapi.exception.ChocolateBadRequestException;
import fr.isencaen.chocolateapi.exception.ChocolateFunctionalException;
import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.exception.ChocolateTechnicalException;
import fr.isencaen.chocolateapi.model.Chocolate;
import fr.isencaen.chocolateapi.model.ChocolatePrice;
import fr.isencaen.chocolateapi.model.ChocolatePriceId;
import fr.isencaen.chocolateapi.model.Dispenser;
import fr.isencaen.chocolateapi.repository.ChocolatePriceRepository;
import fr.isencaen.chocolateapi.repository.ChocolateRepository;
import fr.isencaen.chocolateapi.repository.DispenserRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class PriceService {
    private final ChocolateRepository chocolateRepository;
    private final DispenserRepository dispenserRepository;
    private final ChocolatePriceRepository chocolatePriceRepository;
    private final RatesService ratesService;

    public PriceService(ChocolateRepository chocolateRepository, DispenserRepository dispenserRepository, ChocolatePriceRepository chocolatePriceRepository, RatesService ratesService) {
        this.chocolateRepository = chocolateRepository;
        this.dispenserRepository = dispenserRepository;
        this.chocolatePriceRepository = chocolatePriceRepository;
        this.ratesService = ratesService;
    }

    public BigDecimal getChocolatePrice(Integer dispenserId, Integer chocolateId) throws ChocolateBadRequestException, ChocolateNotFoundException, ChocolateTechnicalException, ChocolateFunctionalException {
        Chocolate chocolate = this.chocolateRepository.findById(chocolateId)
                .orElseThrow(() -> new ChocolateBadRequestException(String.format("The chocolate %s was not found", chocolateId), "CHOCOLATE_NOT_FOUND"));

        Dispenser dispenser = dispenserRepository.findById(dispenserId)
                .orElseThrow(() -> new ChocolateNotFoundException(String.format("The dispenser %s was not found", dispenserId), "DISPENSER_NOT_FOUND"));

        ChocolatePrice chocolatePrice = chocolatePriceRepository.findById(new ChocolatePriceId(chocolate, dispenser))
                .orElseThrow(() -> new ChocolateBadRequestException(String.format("The price of the chocolate %s (dispenser %s) was not found", chocolateId, dispenserId), "PRICE_NOT_FOUND"));

        return chocolatePrice.getPrice();
    }

    public BigDecimal getChocolatePrinceInCurrency(Integer dispenserId, Integer chocolateId, String currency) throws ChocolateBadRequestException, ChocolateNotFoundException, ChocolateTechnicalException, ChocolateFunctionalException {
        BigDecimal chocolatePrice = getChocolatePrice(dispenserId, chocolateId);
        BigDecimal rate = ratesService.getRate(currency);

        return rate.multiply(chocolatePrice);
    }
}
