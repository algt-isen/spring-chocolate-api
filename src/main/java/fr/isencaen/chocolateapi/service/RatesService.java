package fr.isencaen.chocolateapi.service;

import fr.isencaen.chocolateapi.exception.ChocolateFunctionalException;
import fr.isencaen.chocolateapi.exception.ChocolateTechnicalException;
import fr.isencaen.chocolateapi.model.RatesDto;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class RatesService {
    @Cacheable("rates")
    public BigDecimal getRate(String currency) throws ChocolateFunctionalException, ChocolateTechnicalException {
        RestTemplate restTemplate = new RestTemplate();
        RatesDto ratesDto = restTemplate.getForObject("https://cdn.taux.live/api/ecb.json", RatesDto.class);

        if (ratesDto == null || ratesDto.rates() == null || !ratesDto.rates().containsKey("EUR") || ratesDto.rates().get("EUR") == null) {
            throw new ChocolateTechnicalException("Invalid rates", "INVALID_RATES");
        }
        if (!ratesDto.rates().containsKey(currency) || ratesDto.rates().get(currency) == null) {
            throw new ChocolateFunctionalException("The given currency rate is unknown", "UNKNOWN_RATE");
        }

        BigDecimal eurRate = ratesDto.rates().get("EUR").setScale(10, RoundingMode.HALF_DOWN);
        BigDecimal currencyRate = ratesDto.rates().get(currency).setScale(10, RoundingMode.HALF_DOWN);

        return currencyRate.divide(eurRate, RoundingMode.HALF_DOWN);
    }
}
