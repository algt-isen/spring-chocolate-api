package fr.isencaen.chocolateapi.service;

import fr.isencaen.chocolateapi.exception.ChocolateFunctionalException;
import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.model.Chocolate;
import fr.isencaen.chocolateapi.model.ChocolateDto;
import fr.isencaen.chocolateapi.model.CreateChocolateDto;
import fr.isencaen.chocolateapi.repository.ChocolateRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChocolateService {

    private final ChocolateRepository chocolateRepository;

    public ChocolateService(ChocolateRepository chocolateRepository) {
        this.chocolateRepository = chocolateRepository;
    }

    @Cacheable("chocolates")
    public List<ChocolateDto> getAllChocolates() {
        return chocolateRepository.findAll().stream()
                .map(chocolate -> new ChocolateDto(chocolate.getId(), chocolate.getName(), chocolate.getBarCode()))
                .toList();
    }

    public ChocolateDto createChocolate(CreateChocolateDto createChocolateDto) throws ChocolateFunctionalException {
        if (chocolateRepository.existsByBarCode(createChocolateDto.barCode())) {
            throw new ChocolateFunctionalException("duplicate barcode", "DUPLICATE_BARCODE");
        }
        Chocolate newChocolate = new Chocolate(createChocolateDto.name(), createChocolateDto.barCode());
        Chocolate chocolateCreated = chocolateRepository.save(newChocolate);
        return new ChocolateDto(chocolateCreated.getId(), chocolateCreated.getName(), chocolateCreated.getBarCode());
    }

    @CacheEvict(value = "chocolates", allEntries = true)
    public void deleteChocolateById(Integer chocolateId) {
        chocolateRepository.deleteById(chocolateId);
    }

    public ChocolateDto getOneChocolate(Integer chocolateId) throws ChocolateNotFoundException {
        return chocolateRepository.findById(chocolateId)
                .map(chocolate -> new ChocolateDto(chocolate.getId(), chocolate.getName(), chocolate.getBarCode()))
                .orElseThrow(() -> new ChocolateNotFoundException(String.format("The chocolate %s was not found", chocolateId), "CHOCOLATE_NOT_FOUND"));
    }
}
