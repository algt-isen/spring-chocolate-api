package fr.isencaen.chocolateapi.service;

import fr.isencaen.chocolateapi.exception.ChocolateFunctionalException;
import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.model.*;
import fr.isencaen.chocolateapi.repository.ChocolateRepository;
import fr.isencaen.chocolateapi.repository.DispenserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ChocolatePriceService {
    private final DispenserRepository dispenserRepository;
    private final ChocolateRepository chocolateRepository;

    public ChocolatePriceService(DispenserRepository dispenserRepository, ChocolateRepository chocolateRepository) {
        this.dispenserRepository = dispenserRepository;
        this.chocolateRepository = chocolateRepository;
    }

    public ChocolatePriceDto createChocolatePrice(Integer dispenserId, Integer chocolateId, CreateChocolatePriceDto createChocolatePriceDto) throws ChocolateNotFoundException, ChocolateFunctionalException {
        Dispenser dispenser = this.dispenserRepository.findById(dispenserId)
                .orElseThrow(() -> new ChocolateNotFoundException(String.format("The dispenser %s was not found", dispenserId), "DISPENSER_NOT_FOUND"));
        Chocolate chocolate = this.chocolateRepository.findById(dispenserId)
                .orElseThrow(() -> new ChocolateNotFoundException(String.format("The chocolate %s was not found", chocolateId), "CHOCOLATE_NOT_FOUND"));

        if (dispenser.getChocolatePrices().stream().anyMatch(price -> price.getChocolate().equals(chocolate))) {
            throw new ChocolateFunctionalException("Price already exists", "PRICE_DUPLICATE");
        }

        ChocolatePrice chocolatePrice = new ChocolatePrice(chocolate, dispenser, createChocolatePriceDto.price());
        dispenser.getChocolatePrices().add(chocolatePrice);
        this.dispenserRepository.save(dispenser);

        return ChocolatePriceDto.of(chocolatePrice);
    }

    public ChocolatePriceDto getChocolatePrice(Integer dispenserId, Integer chocolateId) throws ChocolateNotFoundException {
        Dispenser dispenser = this.dispenserRepository.findById(dispenserId)
                .orElseThrow(() -> new ChocolateNotFoundException(String.format("The dispenser %s was not found", dispenserId), "DISPENSER_NOT_FOUND"));
        Chocolate chocolate = this.chocolateRepository.findById(dispenserId)
                .orElseThrow(() -> new ChocolateNotFoundException(String.format("The chocolate %s was not found", chocolateId), "CHOCOLATE_NOT_FOUND"));

        Optional<ChocolatePrice> optionalPrice = dispenser.getChocolatePrices().stream()
                .filter(price -> price.getChocolate().equals(chocolate))
                .findFirst();

        return optionalPrice
                .map(ChocolatePriceDto::of)
                .orElseThrow(() -> new ChocolateNotFoundException(String.format("The price for the chocolate %s in the dispenser %s was not found", chocolateId, dispenserId), "PRICE_NOT_FOUND"));
    }
}
