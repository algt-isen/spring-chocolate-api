package fr.isencaen.chocolateapi.service;

import fr.isencaen.chocolateapi.exception.ChocolateNotFoundException;
import fr.isencaen.chocolateapi.model.CreateDispenserDto;
import fr.isencaen.chocolateapi.model.Dispenser;
import fr.isencaen.chocolateapi.model.DispenserDto;
import fr.isencaen.chocolateapi.model.Spiral;
import fr.isencaen.chocolateapi.repository.DispenserRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Service
public class DispenserService {

    private final DispenserRepository dispenserRepository;

    public DispenserService(DispenserRepository dispenserRepository) {
        this.dispenserRepository = dispenserRepository;
    }

    public DispenserDto createDispenser(CreateDispenserDto createDispenserDto) {
        final Dispenser dispenser = new Dispenser();
        dispenser.setChocolatePrices(new ArrayList<>());
        dispenser.setSpirals(
                Stream.generate(
                                () -> new Spiral(dispenser, createDispenserDto.maxSpiralSize(), new ArrayList<>())
                        )
                        .limit(createDispenserDto.spirals())
                        .toList()
        );
        this.dispenserRepository.save(dispenser);
        return DispenserDto.of(dispenser);
    }

    public List<DispenserDto> getAllDispensers() {
        return dispenserRepository.findAll().stream()
                .map(DispenserDto::of)
                .toList();
    }

    public DispenserDto getOneDispenser(Integer dispenserId) throws ChocolateNotFoundException {
        return dispenserRepository.findById(dispenserId)
                .map(DispenserDto::of)
                .orElseThrow(() -> new ChocolateNotFoundException(String.format("The dispenser %s was not found", dispenserId), "DISPENSER_NOT_FOUND"));
    }

    public void deleteDispenserById(Integer dispenserId) {
        dispenserRepository.deleteById(dispenserId);
    }

}
